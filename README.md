* DataUSA is the project name

* DataAPI is the app name 

* Used APIs
  * https://api.quarantine.country/api/v1/summary/latest
    It gives information country wise such as Confirmed, Active, Death, Recovered, Critical, Tested, DeathRatio, RecoveredRatio
  * https://covid-api.mmediagroup.fr/v1/cases?country=India  
    It gives information state wise in the country specified as well as informations like 
    population, sq_km_area, life_expectancy, elevation_in_meters, continent.

* Combined these two APIs to provide all the necessary information to describe the country and its states various covid19 cases.
