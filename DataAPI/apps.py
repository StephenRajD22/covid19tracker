from django.apps import AppConfig


class DataapiConfig(AppConfig):
    name = 'DataAPI'
