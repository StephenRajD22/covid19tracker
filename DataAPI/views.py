import json

from django.http import HttpResponse
import requests
from django.shortcuts import render


def index(request):
    json_res = dict()
    res = requests.get('https://api.quarantine.country/api/v1/summary/latest')
    if request.method == "POST":
        country = request.POST.get('country')
        if country != 'default':
            lowercase = country.lower()
            res1 = requests.get(url='https://covid-api.mmediagroup.fr/v1/cases?country=' + country)
            res1_json = res1.json()
            res_json = res.json()
            abbreviation = res1_json['All']['abbreviation']
            for i in res_json["data"]['regions'].items():
                if i[1]['iso3166a2'] == abbreviation:
                    break
            res1_json['All']['confirmed'] = i[1]['total_cases']
            res1_json['All']['deaths'] = i[1]['deaths']
            res1_json['All']['recovered'] = i[1]['recovered']
            json_res['new'] = res1_json
        else:
            orderby = request.POST.get('orderby')
            if orderby != 'country':
                json_res['sorted'] = sortedData(orderby, res)
                json_res['sortedflag'] = True
            else:
                json_res['flag'] = True
    else:
        json_res['flag'] = True
        json_res['new'] = res.json()
    return render(request, 'DataAPI/Dataarea.html', json_res)


def sortedData(orderby, res):
    sorted_data = sorted(res.json()["data"]['regions'].items(), key=lambda x:x[1][orderby], reverse=True)
    return sorted_data