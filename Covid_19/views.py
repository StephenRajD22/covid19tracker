import json

from django.http import HttpResponse
import requests
from django.shortcuts import render


def index(request):
    if request.method == "GET":
        res = requests.get(url='https://covid-api.mmediagroup.fr/v1/cases')
        data = json.load(res.json())
        for i in data:
            print(i['All']['country'])

    return render(request, 'Covid_19/Dataarea.html', data)
